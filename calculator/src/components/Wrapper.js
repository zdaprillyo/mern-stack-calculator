import "./Wrapper.css";
import React, { useState } from "react";

const Wrapper = ({ children }) => {
  let [logged, setLogged] = useState(false);
  let [username, setUsername] = useState("");
  let [password, setPassword] = useState("");

  const usernameHandler = (e) => {
    setUsername(e.target.value)
  };

  const passwordHandler = (e) => {
    setPassword(e.target.value)
  };

  const loginHandler = (e) => {
    e.preventDefault();
  };
  const logoutHandler = (e) => {
    e.preventDefault();
    setLogged(false)
  };
  return (
  <div className="form">
    <form>
      <div className="login" style={ !logged ? {} : {display: 'none'} }>
        <input type="text" className="form-control" name="username" placeholder="username" value={username} onChange={usernameHandler}/>
        <input type="password" className="form-control" name="password" placeholder="password" value={password} onChange={passwordHandler}/>
        <input type="button" className="btn btn-sm btn-success" value="Login" onClick={loginHandler}/>
      </div>
      <input type="button" className="btn btn-sm btn-warning" value="Logout" style={ logged ? {} : {display: 'none'} } onClick={logoutHandler}/>
      <div className="wrapper" style={ logged ? {} : {display: 'none'} }>
        {children}
      </div>
    </form>
  </div>);
};
export default Wrapper;
